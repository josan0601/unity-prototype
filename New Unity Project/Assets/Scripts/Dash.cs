﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dash : StateMachineBehaviour
{
    GameObject Player;
    Text PrintText;
    Transform target;
    float StartPosition;
    float Timer = 0;
    public float Distance = 5.0f;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PrintText = GameObject.Find("PrintText").GetComponent<Text>();
        Player = animator.gameObject;
        target = PlayerManager.instance.player.transform;
        StartPosition = PlayerManager.instance.player.transform.position.x;
        if(PlayerManager.instance.player.GetComponent<SpriteRenderer>().flipX == false)
            PlayerManager.instance.player.GetComponent<Rigidbody2D>().velocity = new Vector2(50.0f, 0.0f);
        else
            PlayerManager.instance.player.GetComponent<Rigidbody2D>().velocity = new Vector2(-50.0f, 0.0f);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Timer += Time.deltaTime;
        if(Mathf.Abs(target.position.x - StartPosition) >= Distance || Timer > 1.0f)
            animator.SetInteger("State", 0);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("CanDash", true);
        PlayerManager.instance.player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
