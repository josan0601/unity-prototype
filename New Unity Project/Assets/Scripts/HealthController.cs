using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController
{
    //public event EventHandler OnHealthChanged;

    private int health;
    private int maxhp;

    public HealthController(int maxhp)
    {
        this.maxhp = maxhp;
        health = maxhp;
    }
    public int GetHP()
    {
        return health;
    }
    public float NormalizeHp()
    {
        return (float)health / maxhp;
    }
    public void Dmg(int dmg)
    {
        health -= dmg;
        if (health < 0) health = 0;
    }
    public void Heal(int hp)
    {
        health += hp;
        if (health > maxhp) health = maxhp;
    }
}

