using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    public HealthBar hpbar;

    public void Start()
    {
        HealthController healthBar = new HealthController(100);
        hpbar.Setup(healthBar);
    }
}
