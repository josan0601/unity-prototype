﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

    public Transform Mytr;
    public Transform Tr;
    bool Attacking = false;
    bool Started = false;
    int Alpha = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.E) && !Attacking)
        {
            Attacking = true;
            Started = false;
        }

        if (Attacking)
        {
            AttackNormal();
        }
	}

    void StartAttack()
    {
        Mytr.RotateAround(Mytr.parent.position, new Vector3(0.0f, 0.0f, 1.0f), 90);
        Started = true;
    }

    void AttackNormal()
    {
            if (!Started)
                StartAttack();
            
        Mytr.RotateAround(Mytr.parent.position, new Vector3(0.0f, 0.0f, 1.0f), -Alpha);

        if (Mytr.rotation.z <= 0)
            Attacking = false;
    }
}
