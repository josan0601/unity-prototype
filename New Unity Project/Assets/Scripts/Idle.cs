﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Idle : StateMachineBehaviour
{
    public PlayerController Controls;
    GameObject Player;
    Text PrintText;
    public float InputThreshold = 0.1f;

     //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PrintText = GameObject.Find("PrintText").GetComponent<Text>();
        Player = animator.gameObject;
        Controls = new PlayerController();
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        if (Input.GetAxis("Horizontal") > InputThreshold || Input.GetAxis("Horizontal") < -InputThreshold)
        {
            animator.SetInteger("State", 1);
            return;
        }


        if (Input.GetButton("Hit"))
        {
            animator.SetInteger("State", 3);
            return;
        }


        if (Input.GetButton("Jump"))
        {
            animator.SetInteger("State", 2);
            return;
        }

        if (Input.GetButton("Dash"))
        { 
            animator.SetInteger("State", 9);
            return;
        }
    }

     //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
