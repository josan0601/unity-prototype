using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeBehaviour : MonoBehaviour
{
    public Transform transform;

    public float ShakeAmount = 0;
    public float shakeDuration = 0f;
    public float shakeMagnitude = 0.7f;
    public float dampingSpeed = 1.0f;
    public Transform Target;


    Vector3 initialPosition;
    void Awake()
    {
        /*if (transform == null)
        {
            transform = GetComponent(typeof(Transform)) as Transform;
        }*/
    }
 
    void Update()
    {
        float quakeAmt = Random.value;

        if (shakeDuration > 0)
        {
            transform.localPosition = transform.localPosition * quakeAmt;
            shakeDuration -= Time.deltaTime * dampingSpeed;
        }
        else
        {  
            shakeDuration = 0f;
        }
        if (Input.GetKey(KeyCode.L))
            shakeDuration = .2f;
    }
    
    
}
