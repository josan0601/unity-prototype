using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    public HealthController hpsystem;

    public void Setup(HealthController hpbar)
    {
        this.hpsystem = hpbar;
    }
    private void Update()
    {
        transform.Find("Bar").localScale = new Vector3(-hpsystem.NormalizeHp(), 1f);
        if (Input.GetKey(KeyCode.N))
        {
            hpsystem.Dmg(1);
        }
        if (Input.GetKey(KeyCode.M))
        {
            hpsystem.Heal(1);
        }
    }
}

