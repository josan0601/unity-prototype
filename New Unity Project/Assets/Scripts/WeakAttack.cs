﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeakAttack : StateMachineBehaviour
{
    public PlayerController Controls;
    GameObject Player;
    Text PrintText;
    bool HasDamaged;
    Transform target;
    bool right;
    public float timer = 0.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PrintText = GameObject.Find("PrintText").GetComponent<Text>();
        Player = animator.gameObject;
        HasDamaged = false;
        right = true;
        target = PlayerManager.instance.player.transform;
        animator.SetInteger("State", 0);
        //Player.GetComponent<SpriteRenderer>().flipY = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RaycastHit2D Hit;
        timer += Time.deltaTime;

        if (Player.GetComponent<SpriteRenderer>().flipX)
        {
            Hit = Physics2D.Raycast(new Vector2(target.position.x - (target.localScale.x / 2) - 2, target.position.y), Vector2.left, 1.0f);
            right = false;
        }
        else
        {
            Hit = Physics2D.Raycast(new Vector2(target.position.x + (target.localScale.x / 2) + 2, target.position.y), Vector2.right, 1.0f);
            right = true;
        }


        if (Hit && !HasDamaged)
        {
            HasDamaged = true;
            
            if (Hit.collider.gameObject.tag == "Enemy")
            {
                Hit.collider.gameObject.GetComponent<EnemyController>().Health -= 20;

                if (right)
                {
                    Hit.transform.position += new Vector3(0.5f, 0.0f, 0.0f);
                }
                else
                    Hit.transform.position -= new Vector3(0.5f, 0.0f, 0.0f);
                
            }
        }

        

        if (animator.GetInteger("State") == 0)
        {
            if (Input.GetButtonDown("Hit"))
                animator.SetInteger("State", 4);

            //if (Input.GetKeyDown(KeyCode.G))
            //    animator.SetInteger("State", 6);

            //PrintText.text = "1st Weak Attack";
        }

        if (Input.GetButton("Dash"))
        {
            animator.SetInteger("State", 9);
            return;
        }

        if (timer >= 1.0f && animator.GetInteger("State") != 4)
        {
            animator.SetInteger("State", 0);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Player.GetComponent<SpriteRenderer>().flipY = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
