﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EnemyController : MonoBehaviour
{
    public float radious = 10.0f;
    public float Distance = 2.0f;
    public float speed = 10.0f;
    public float stopDis = 2.0f;
    public float Health = 200.0f;
    public GameObject MyKeeper;
   
    public Rigidbody2D rb;
    public float TimeNextToPlayer = 2.0f;
    float internaltimer;

    public float dmg;
    BoxCollider2D mycol;
    Transform target;
    // Start is called before the first frame update
    void Start()
    {
        MyKeeper = GameObject.Find("Keeper");
        target = PlayerManager.instance.player.transform;
        rb = this.GetComponent<Rigidbody2D>();
        mycol = GetComponent<BoxCollider2D>();
        internaltimer = TimeNextToPlayer;
        
    }
    // Update is called once per frame
    void Update()
    {
        if (Health <= 0)
            Destroy(this.gameObject);
        //vector from enemy to players right
        Vector3 directionRight = new Vector3((target.position.x + target.right.x * Distance) - transform.position.x,
                                             (target.position.y + target.right.y * Distance) - transform.position.y,
                                             (target.position.z + target.right.z * Distance) - transform.position.z);
        //ector from enemy to players left
        Vector3 directionLeft = new Vector3((target.position.x - target.right.x * Distance) - transform.position.x,
                                            (target.position.y - target.right.y * Distance) - transform.position.y,
                                            (target.position.z - target.right.z * Distance) - transform.position.z);
        Vector3 direction = new Vector3(0.0f, 0.0f, 0.0f);
        //check closest point
        if (directionLeft.magnitude < directionRight.magnitude)
        {
            direction = directionLeft;
        }
        else
        {
            direction = directionRight;
        }
        float distance = Vector2.Distance((new Vector2(target.position.x, target.position.y)),
                                          (new Vector2(transform.position.x, transform.position.y)));
        if (distance <= radious)
        {
            if (direction.y > 0)
                direction.y = 0;
            rb.velocity = direction * speed;
        }
        //ENEMY DISTANCE CHECK
        Vector2 offset = new Vector2(0.0f, 0.0f);
        if (rb.velocity.x > 0)
        {
            offset.x = transform.localScale.x / 2 + 0.01f;
        }
        else
        {
            offset.x = -transform.localScale.x / 2 - 0.01f;
        }
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x + offset.x, transform.position.y + offset.y), new Vector2(rb.velocity.x, 0), stopDis);
        Debug.DrawRay(new Vector2(transform.position.x + offset.x, transform.position.y + offset.y), new Vector2(rb.velocity.x, 0).normalized, Color.green);
        if (hit.collider && hit.collider.tag == "Enemy")
        {
            rb.velocity = new Vector2(0.0f, rb.velocity.y);
            print(hit.collider.name);
        }

        if (hit.collider && hit.collider.tag == "Player" )
        {

            TimeNextToPlayer -= Time.deltaTime;
        
            if(TimeNextToPlayer <= 0)
            {
                //print("DEBUG");

                //GameObject.Find("Bar").GetComponent<HPBar>().scalarX += 10;

                MyKeeper.GetComponent<Keeper>().Life -= 30.0f;


                TimeNextToPlayer = internaltimer;
            }
        
        }
        else
        {
            TimeNextToPlayer = internaltimer;
        }
    }
}