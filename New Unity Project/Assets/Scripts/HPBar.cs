﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBar : MonoBehaviour
{
    RectTransform rectTransform;
    float PlayerLife;
    public float scalarX = -590.0f;
    public float scalarY = 10f;
    //Use this for initialization
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        //GUI.Label(new Rect(65, 34, 499, 632), "Rect : " + rectTransform.rect);

    }
    //Update is called once per frame
    void Update()
    {

        PlayerLife = GameObject.Find("Keeper").GetComponent<Keeper>().Life;

        GetComponent<RectTransform>().sizeDelta = new Vector2(scalarX, scalarY);
        if (scalarX >= -780)
            scalarX = -780;
        if (scalarX <= -870)
            scalarX = -870;

        if (Input.GetKey(KeyCode.M))
        {
            scalarX -= 10;
            //scalarY -= 10;
        }
        if (Input.GetKey(KeyCode.N))
        {
            scalarX += 10;
            //scalarY += 10;
        }

        scalarX = PlayerLife;
        /*
        if (PlayerLife != 0.0f)
        {
            scalarX -= PlayerLife;
            PlayerLife = 0.0f;
        }*/
    }
}