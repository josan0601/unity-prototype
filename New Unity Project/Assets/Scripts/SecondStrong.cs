﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecondStrong : StateMachineBehaviour
{
    public PlayerController Controls;
    GameObject Player;
    Text PrintText;
    bool HasDamaged;
    Transform target;
    bool right;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PrintText = GameObject.Find("PrintText").GetComponent<Text>();
        Player = animator.gameObject;
        animator.SetInteger("State", 0);
        HasDamaged = false;
        right = true;
        target = PlayerManager.instance.player.transform;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RaycastHit2D Hit;

        if (Player.GetComponent<SpriteRenderer>().flipX)
        {
            Hit = Physics2D.Raycast(new Vector2(target.position.x - (target.localScale.x / 2) - 2, target.position.y), Vector2.left, 1.0f);
            right = false;
        }
        else
        {
            Hit = Physics2D.Raycast(new Vector2(target.position.x + (target.localScale.x / 2) + 2, target.position.y), Vector2.right, 1.0f);
            right = true;
        }

        if (Hit && !HasDamaged)
        {
            
            if (Hit.collider.gameObject.tag == "Enemy")
            {
                Hit.collider.gameObject.GetComponent<EnemyController>().Health -= 60;
                HasDamaged = true;

                if (right)
                {
                    Hit.transform.position += new Vector3(2.0f, 0.0f, 0.0f);
                }
                else
                    Hit.transform.position -= new Vector3(2.0f, 0.0f, 0.0f);
                
            }
        }


        if (Input.GetButtonDown("Dash"))
            animator.SetInteger("State", 9);

        PrintText.text = "Second Strong Attack";
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
