﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : StateMachineBehaviour
{
    public PlayerController Controls;
    GameObject Player;
    public float inputThreshold = 0.1f;
    public float Speed = 7.0f;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Player = animator.gameObject;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (Input.GetKey(KeyCode.A))
        if (Input.GetAxis("Horizontal") <= -inputThreshold)
        {
            //Player.GetComponent<Rigidbody2D>().velocity = ((new Vector3(1.0f, 0.0f, 0.0f)) * -10.0f);
            Player.transform.Translate(-Speed * Time.deltaTime, 0, 0);
            Player.GetComponent<SpriteRenderer>().flipX = true;
        }

        //if (Input.GetKey(KeyCode.D))
        if (Input.GetAxis("Horizontal") >= inputThreshold)
        {
            //Player.GetComponent<Rigidbody2D>().velocity = ((new Vector3(1.0f, 0.0f, 0.0f)) * 10.0f);
            Player.transform.Translate(Speed * Time.deltaTime, 0, 0);
            Player.GetComponent<SpriteRenderer>().flipX = false;
        }

        if (Input.GetButton("Jump"))
        {
            animator.SetInteger("State", 2);
            return;
        }

        if (Input.GetAxis("Horizontal") >= -inputThreshold && Input.GetAxis("Horizontal") <= inputThreshold && animator.GetInteger("State") != 3)
        {
            animator.SetInteger("State", 0);
            return;
        }

        if (Input.GetButton("Hit"))
        {
            animator.SetInteger("State", 3);
            return;
        }


        if (Input.GetButton("Dash"))
        {
            animator.SetInteger("State", 9);
            return;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
