﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic : MonoBehaviour {

    public float time;

    float timerup;
    float timerdown;

    bool change;

    // Use this for initialization
    void Start () {
        timerup = time;
        timerdown = time;
        change = false;

        print(time);
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        if(timerup >= 0 && change)
        {
            transform.Translate(0, 1f * Time.deltaTime, 0);

            timerup -= Time.deltaTime;

            print("going up");
        }

        if (timerup <= 0)
        {
            timerup = time;
            change = !change;
        }

        if(timerdown > 0 && change == false)
        {
            transform.Translate(0, -1f * Time.deltaTime, 0);

            timerdown -= Time.deltaTime;

            print(timerdown);
            //print("going down");
        }

        if (timerdown < 0)
        {
            print("stop going down");
            timerdown = time;
            change = !change;
        }

    }
}
