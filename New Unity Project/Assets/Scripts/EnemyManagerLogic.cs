﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManagerLogic : MonoBehaviour {

    public Transform spawnPos;
    public GameObject enemy;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        SpawnEnemy();
	}

    void SpawnEnemy()
    {
        if (Input.GetKey(KeyCode.N))
        {
            Instantiate(enemy, spawnPos.position, spawnPos.rotation);
        }
    }
}
