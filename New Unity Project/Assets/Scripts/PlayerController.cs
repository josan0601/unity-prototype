// GENERATED AUTOMATICALLY FROM 'Assets/Player Controller.inputactions'

using System;
using UnityEngine;
using UnityEngine.Experimental.Input;


namespace UnityEngine
{
    [Serializable]
    public class PlayerController : InputActionAssetReference
    {
        public PlayerController()
        {
        }
        public PlayerController(InputActionAsset asset)
            : base(asset)
        {
        }
        private bool m_Initialized;
        private void Initialize()
        {
            // Movement
            m_Movement = asset.GetActionMap("Movement");
            m_Movement_Move = m_Movement.GetAction("Move");
            m_Movement_Jump = m_Movement.GetAction("Jump");
            m_Movement_Attack = m_Movement.GetAction("Attack");
            m_Movement_Dash = m_Movement.GetAction("Dash");
            m_Initialized = true;
        }
        private void Uninitialize()
        {
            m_Movement = null;
            m_Movement_Move = null;
            m_Movement_Jump = null;
            m_Movement_Attack = null;
            m_Movement_Dash = null;
            m_Initialized = false;
        }
        public void SetAsset(InputActionAsset newAsset)
        {
            if (newAsset == asset) return;
            if (m_Initialized) Uninitialize();
            asset = newAsset;
        }
        public override void MakePrivateCopyOfActions()
        {
            SetAsset(ScriptableObject.Instantiate(asset));
        }
        // Movement
        private InputActionMap m_Movement;
        private InputAction m_Movement_Move;
        private InputAction m_Movement_Jump;
        private InputAction m_Movement_Attack;
        private InputAction m_Movement_Dash;
        public struct MovementActions
        {
            private PlayerController m_Wrapper;
            public MovementActions(PlayerController wrapper) { m_Wrapper = wrapper; }
            public InputAction @Move { get { return m_Wrapper.m_Movement_Move; } }
            public InputAction @Jump { get { return m_Wrapper.m_Movement_Jump; } }
            public InputAction @Attack { get { return m_Wrapper.m_Movement_Attack; } }
            public InputAction @Dash { get { return m_Wrapper.m_Movement_Dash; } }
            public InputActionMap Get() { return m_Wrapper.m_Movement; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled { get { return Get().enabled; } }
            public InputActionMap Clone() { return Get().Clone(); }
            public static implicit operator InputActionMap(MovementActions set) { return set.Get(); }
        }
        public MovementActions @Movement
        {
            get
            {
                if (!m_Initialized) Initialize();
                return new MovementActions(this);
            }
        }
    }
}
