﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : StateMachineBehaviour
{
    public PlayerController Controls;
    GameObject Player;
    public float InputThreshold = 0.1f;
    public float Speed = 7.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Player = animator.gameObject;
        //Player.GetComponent<Rigidbody2D>().AddForce( (new Vector3(0.0f,1.0f,0.0f)) * 500.0f);
        Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 10.0f);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Input.GetAxis("Horizontal") <= -InputThreshold)
        {
            //Player.GetComponent<Rigidbody2D>().velocity = ((new Vector3(1.0f, 0.0f, 0.0f)) * -10.0f);
            Player.transform.Translate(-Speed * Time.deltaTime, 0, 0);
            Player.GetComponent<SpriteRenderer>().flipX = true;
        }
        
        if (Input.GetAxis("Horizontal") >= InputThreshold)
        {
            //Player.GetComponent<Rigidbody2D>().velocity = ((new Vector3(1.0f, 0.0f, 0.0f)) * 10.0f);
            Player.transform.Translate(Speed * Time.deltaTime, 0, 0);
            Player.GetComponent<SpriteRenderer>().flipX = false;
        }

        if (Player.GetComponent<Rigidbody2D>().velocity.y == 0)
        {
            animator.SetInteger("State", 0);
            if (Input.GetAxis("Horizontal") <= -InputThreshold || Input.GetAxis("Horizontal") >= InputThreshold)
                animator.SetInteger("State", 1);
            return;
        }

        if (Input.GetButton("Dash"))
        {
            animator.SetInteger("State", 9);
            return;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
