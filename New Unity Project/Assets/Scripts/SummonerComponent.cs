﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SummonerComponent : MonoBehaviour
{
    public GameObject SpawnObj;
    public float SpawnDistance = 100;
    public float SpawnTime = 2;
    float time;
    // Start is called before the first frame update
    void Start()
    {
        time = SpawnTime;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - (transform.localScale.y / 2) - 1), Vector2.down, SpawnDistance);
        SpawnTime -= Time.deltaTime;
        if (SpawnTime < 0)
        {
            SpawnTime = time;
            if (hit)
            {
                //Debug.Log(hit.transform.name);
                Instantiate(SpawnObj, hit.point, new Quaternion(0, 0, 0, 1));
            }
            else
            {
            }
        }
    }
}
