using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomInOut : MonoBehaviour
{
    private int zoom;
    public int zoomSpeed;
    public int maxZoom;
    public int minZoom;
    // Start is called before the first frame update
    void Start()
    {
        
        GetComponent<Camera>().orthographicSize = 10;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q)) zoom = zoomSpeed;
        else if (Input.GetKey(KeyCode.E)) zoom = -zoomSpeed;

        if(Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.E))
        {
            GetComponent<Camera>().orthographicSize = GetComponent<Camera>().orthographicSize + zoom * 0.01f;
            if (GetComponent<Camera>().orthographicSize >= maxZoom)
                GetComponent<Camera>().orthographicSize = maxZoom; // Max size
            else if (GetComponent<Camera>().orthographicSize <= minZoom)
                GetComponent<Camera>().orthographicSize = minZoom; // Min size 
        }
       
    }
}

